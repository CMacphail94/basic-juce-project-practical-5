/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    textButton1.setButtonText ("Click Me");
    textButton2.setButtonText ("Or Me");
    slider1.setSliderStyle (Slider::LinearHorizontal);

    addAndMakeVisible (&textButton1);
    textButton1.addListener(this);
    addAndMakeVisible (&textButton2);
    textButton2.addListener(this);
    addAndMakeVisible (&slider1);
    slider1.addListener(this);
    

}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    
    DBG(getWidth());
    DBG(getHeight());
    textButton1.setBounds (10, 10, getWidth() - 20, 40);
    textButton2.setBounds (10, 90, getWidth() - 20, 40);
    slider1.setBounds (10, 120, getWidth() - 20, 40);
    
    
}

void MainComponent::paint (Graphics& g)
{
    g.setColour(Colours::cadetblue);
    g.fillRect(0, 0, getWidth()/2.0, getHeight()/2.0);
    g.fillRect(250,200,getWidth()/2.0, getHeight()/2.0);
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &textButton1)
        DBG ("textButton1 Clicked!\n");
    else if (button == &textButton2)
        DBG ("textButton2 Clicked!\n");
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    DBG ("Slider changed");
}
